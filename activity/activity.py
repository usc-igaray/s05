from abc import ABC, abstractclassmethod

class Animal(ABC):
    @abstractclassmethod
    def eat(self, food):
        pass
    @abstractclassmethod
    def make_sound(self):
        pass

class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age
        
    def get_name(self):
        print(f'Name of Animal is {self._name}')
    def set_name(self, name):
        self._name = name
        
    def get_breed(self):
        print(f'Breed of Animal is {self._breed}')
    def set_breed(self, breed):
        self._breed = breed
        
    def get_age(self):
        print(f'Age of Animal is {self._age}')
    def set_age(self, age):
        self._age = age
        
    def eat(self, food):
        print(f'Eaten {food}')
        
    def make_sound(self):
        print('Meow!')
    def call(self):
        print(f'{self._name} snacks is here!')



class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()
        self._name = name
        self._breed = breed
        self._age = age
        
    def get_name(self):
        print(f'Name of Animal is {self._name}')
    def set_name(self, name):
        self._name = name
        
    def get_breed(self):
        print(f'Breed of Animal is {self._breed}')
    def set_breed(self, breed):
        self._breed = breed
        
    def get_age(self):
        print(f'Age of Animal is {self._age}')
    def set_age(self, age):
        self._age = age
        
    def eat(self, food):
        print(f'Swallowed {food}')
        
    def make_sound(self):
        print('Arf!')
    def call(self):
        print(f'{self._name}, come here boy!')
        
def func_polymorphism(obj):
    obj.eat('snackbytes')
    obj.make_sound()
    obj.call()
    print()
        
cat = Cat("Chesh", "Cheshire", 9)
dog = Dog("Doggo", "Dog", 7)

func_polymorphism(cat)
func_polymorphism(dog)
