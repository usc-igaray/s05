class Person:
    def __init__(self, year = 2022):
        self._name = "Idan"
        self.year = year
        self._age = 0
        
    def get_name(self):
        print(f'Name of Person: {self._name}')
        return self._name
    def set_name(self, name):
        self._name = name
        
    def get_age(self):
        print(f'Age of Person: {self._age}')
        return self._age
    def set_age(self, age):
        self._age = age

p1 = Person(2023)
p1.set_name("Gnarl")
p1.set_age(79)
name = p1.get_name()
age = p1.get_age()
p2 = Person(2017)
p2.set_age(77)
age = p2.get_age()
name = p2.get_name()
print(f'Name of Person: {name}')
print(f'Age is {age}')



class Employee(Person):
    def __init__(self):
        super().__init__()
        self._employeeId = "emp-001"
        
    def get_employeeId(self):
        print(f'The employee id is {self._employeeId}')
        return self._employeeId
    def set_employeeId(self, employeeId):
        self._employeeId = employeeId
        
    def get_details(self):
        print(f"{self._employeeId} belongs to {self._name}")

employee = Employee()
employee.get_details()
employee.get_name()

print()
# Mini exercise
    # 1. Create a new class called Student that inherits Person with the additional attributes and methods
    # attributes: Student No, Course, Year Level
    # methods:
    #   Create the necessary getters and setters for each attribute
    #   get_detail: prints the output "<Student name> is currently in year <year level> taking up <Course>"
    
class Student(Person):
    def __init__(self, studentNo, course, yearLevel):
        super().__init__()
        self._studentNo = studentNo
        self._course = course
        self._yearLevel = yearLevel
        
    def get_studentNo(self):
        print(f'The student number is {self._studentNo}')
        return self._studentNo
    
    def set_studentNo(self, studentNo):
        self._studentNo = studentNo
        
    def get_course(self):
        print(f'The course is {self._course}')
        return self._course
    
    def set_course(self, course):
        self._course = course
        
    def get_yearLevel(self):
        print(f'The year level is {self._yearLevel}')
        return self._yearLevel
    
    def set_yearLevel(self, yearLevel):
        self._yearLevel = yearLevel
    
    def get_details(self):
        print(f"{self._studentNo} belongs to {self._name} with {self._course} and  {self._yearLevel}")
    
student = Student("stud-001", "Engineering 101", "4")
student.get_details()



class Admin():
    def is_admin(self):
        print(True)
    
    def user_type(self):
        print('Admin User')
        
class Customer(Admin):
    def is_admin(self):
        print(False)
    
    def user_type(self):
        print('Customer User')

def test_function(obj):
    print()
    obj.is_admin()
    obj.user_type()
        
a1 = Admin()
c1 = Customer()
test_function(a1)
test_function(c1)